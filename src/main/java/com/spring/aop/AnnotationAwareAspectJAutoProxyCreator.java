package com.spring.aop;

import com.rqx.service.RqxBeanPostProcessor;
import com.spring.allInterface.BeanPostProcessor;


import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class  AnnotationAwareAspectJAutoProxyCreator implements BeanPostProcessor {
    private ConcurrentHashMap<String, List<MethodClassInfo>> beforeMethodMap;
    private ConcurrentHashMap<String,List<MethodClassInfo>> afterMethodMap;
    public void setBeforeMethodMap(ConcurrentHashMap<String, List<MethodClassInfo>> beforeMethodMap) {
        this.beforeMethodMap = beforeMethodMap;
    }
    public void setAfterMethodMap(ConcurrentHashMap<String, List<MethodClassInfo>> afterMethodMap) {
        this.afterMethodMap = afterMethodMap;
    }


    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        return bean;
    }


    //AOP操作：对Bean处理
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        System.out.println(beanName);
        LinkedList<String> beforeMethods=new LinkedList<>();//这个bean的所有before事务
        LinkedList<String> afterMethods=new LinkedList<>();//这个bean的所有after事务
        for (String key:beforeMethodMap.keySet()){
            String[] beanName_Method=key.split("\\.");//userService和queryUser
            if(beanName_Method[0].equals(beanName)){
                beforeMethods.add(beanName_Method[1]);
            }

        }
        for (String key:afterMethodMap.keySet()){
            String[] beanName_Method=key.split("\\.");//userService和queryUser
            if(beanName_Method[0].equals(beanName)){
                afterMethods.add(beanName_Method[1]);
            }

        }
        if (!beforeMethods.isEmpty()||!afterMethods.isEmpty()) {
            System.out.println("处理aop");
            //有一个不为空，就AOP
            //这里用JDK(需要接口)     【 CGlib（需要jar包）

            Object proxyInstance = Proxy.newProxyInstance(
                    bean.getClass().getClassLoader(),//1. 类加载器
                    bean.getClass().getInterfaces(),//2. 目标类的核心功能pointcut
                    new InvocationHandler() { //拦截
                        @Override
                        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                            if(beforeMethods.contains(method.getName())){
                                //如果这个pointcut，有before事务
                                //userService.queryUser
                                String thekey=new StringBuilder(beanName).append('.').append(method.getName()).toString();
                                //根据这个pointcut找到所有的前置事务，并调用
                                if(beforeMethodMap.containsKey(thekey)){
                                    for(MethodClassInfo methodClassInfo:beforeMethodMap.get(thekey)){
                                        //eg：实例化了log。 调用logBefore方法
                                        Object instance=methodClassInfo.getClazz().getDeclaredConstructor().newInstance();
                                        methodClassInfo.getMethod().invoke(instance);
                                    }

                                }
                            }
                            Object result= method.invoke(bean, args);//执行userService.queryUser的方法
                            if(afterMethods.contains(method.getName())){
                                //如果这个pointcut，有before事务
                                //userService.queryUser
                                String thekey=new StringBuilder(beanName).append('.').append(method.getName()).toString();
                                //根据这个pointcut找到所有的前置事务，并调用
                                if(afterMethodMap.containsKey(thekey)){
                                    for(MethodClassInfo methodClassInfo:afterMethodMap.get(thekey)){
                                        //eg：实例化了log。 调用logBefore方法
                                        Object instance=methodClassInfo.getClazz().getDeclaredConstructor().newInstance();
                                        methodClassInfo.getMethod().invoke(instance);
                                    }

                                }
                            }
                            return result;
                        }
                    });//3. 辅助功能

            // 返回的代理对象（实现目标的接口），增添辅助功能，（但没有目标对象的属性值信息，是新的对象）
            return proxyInstance; //替换对象
        }

        return bean;
    }
}
