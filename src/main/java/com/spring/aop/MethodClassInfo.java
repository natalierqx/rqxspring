package com.spring.aop;

import java.lang.reflect.Method;

public class MethodClassInfo {
    private Class clazz;
    private Method method;

    public MethodClassInfo(Class clazz, Method method) {
        this.clazz = clazz;
        this.method = method;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    @Override
    public String toString() {
        return "MethodClassInfo{" +
                "clazz=" + clazz +
                ", method=" + method +
                '}';
    }
}
