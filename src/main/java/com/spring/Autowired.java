package com.spring;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)//retention注解？？？？？？？？？？？？？？？？？？？
@Target({ElementType.METHOD,ElementType.FIELD})//ComponentScan注解能写在‘方法和属性’上面
public @interface Autowired {
    String value() default ""; //定义属性，默认值为空
    boolean required() default true;

}
