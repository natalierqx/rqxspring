package com.spring;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)//retention注解？？？？？？？？？？？？？？？？？？？
@Target(ElementType.TYPE)//ComponentScan注解只能写在‘类’上面
public @interface Scope {
    String value() default ""; //定义属性，默认值为空
}
