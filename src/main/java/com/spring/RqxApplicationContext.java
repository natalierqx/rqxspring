package com.spring;

// 这是容器


/* 对应调用代码
* AnnotationConfigApplicationContext applicationContext=new AnnotationConfigApplicationContext(AppConfig.class);
  applicationContext.getBean()
 * 实现：
 * 1. 拿到配置
 * 2. 并解析配置
 * 3. 扫描
 * 4.
 * */


import com.spring.allInterface.BeanNameAware;
import com.spring.allInterface.BeanPostProcessor;
import com.spring.allInterface.InitializeBean;
import com.spring.allInterface.ObjectFactory;
import com.spring.aop.*;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class RqxApplicationContext {
    private Class configClass;//配置文件的类型是Class

    private ConcurrentHashMap<String,BeanDefinition> beanDefinitionMap=new ConcurrentHashMap<>();//存bean的‘定义’
    private ConcurrentHashMap<String,Object> singletonObjects=new ConcurrentHashMap<>();//单例池
    private ConcurrentHashMap<String,Object> earlySingletonObjects=new ConcurrentHashMap<>();//二级缓存
    private ConcurrentHashMap<String, ObjectFactory> singleFactories=new ConcurrentHashMap<>();
    private List<String> creatingBeanNameList=new LinkedList<>();

    private List<BeanPostProcessor> beanPostProcessorList=new ArrayList<>();

    //beforeMethodMap: <pointcut业务,[aspect1,aspect2事务...]>
    private ConcurrentHashMap<String,List<MethodClassInfo>> beforeMethodMap=new ConcurrentHashMap<>();
    private ConcurrentHashMap<String,List<MethodClassInfo>> afterMethodMap=new ConcurrentHashMap<>();


    public RqxApplicationContext(Class configClass) {
        //1.拿到配置
        System.out.println("=================配置================");
        this.configClass=configClass;


        //2.解析扫描注释: 把ComponentScan注解相关信息解析---拿到扫描路径
        System.out.println("===扫描配置文件==");
        if (configClass.isAnnotationPresent(ComponentScan.class)) {
            scan(configClass);
        }

        // *判断是否开启AOP
        System.out.println("-----检查aop---");
        checkAOP();

        // 3.创建
        System.out.println("===创建Bean===");
        for(String beanName: beanDefinitionMap.keySet()){
            BeanDefinition beanDefinition=beanDefinitionMap.get(beanName);
            if(beanDefinition.getScope().equals("singleton")){
                System.out.println("单例:"+beanName);
                Object object=getBean(beanName);//创bean对象
//                singletonObjects.put(beanName,object);//放进单例池
            }
            else{
                System.out.println("多例:"+beanName);
                System.out.println("用的时候再创");
            }
        }

        System.out.println("--------------配置文件 结束-------------\n\n");
    }



    public Object createBean(String beanName,BeanDefinition beanDefinition){
        //！！！根据bean定义创建Bean对象:
        //构造(根据目标类的无参构造函数创建)---set依赖注入(从容器里找bean赋值)
        // ---后处理1(eg:连数据库/运行目标类的某方法@postConstruct)---init初始化---后处理2---return--destory销毁
        System.out.println("创:"+beanName);
        Class clazz= beanDefinition.getClazz();
        try {
            //1.实例化（普通对象）
            System.out.println("1.实例化");
            Object instance = clazz.getDeclaredConstructor().newInstance();//调用这个类的 ’无参数的构造方法，没赋值‘ 返回实例对象
            creatingBeanNameList.add(beanName);//这个正在创
            Object originalInstance=instance;
            singleFactories.put(beanName,() -> getEarlyBeanReference(beanName, originalInstance));

            //2. 依赖注入
            //找类的属性
            for (Field declaredField : clazz.getDeclaredFields()) {
                System.out.println("2.依赖注入");
                //@Autoward的注入流程(基于属性类型/名字赋值)
                if (declaredField.isAnnotationPresent(Autowired.class)) {
                    Object bean = getBean(declaredField.getName());//容器中找到bean
                    if (bean == null) {
                        Autowired autowiredAnnotation = declaredField.getDeclaredAnnotation(Autowired.class);
                        if(autowiredAnnotation.required()) throw new NullPointerException("没找到bean");
                    }
                    System.out.println("   注入"+declaredField);
                     //根据属性名找到,容器里的相应id的bean对象赋值
                    declaredField.setAccessible(true);//破坏属性私有
                    declaredField.set(instance, bean);
                    //给当前对象instance的属性declared属性，赋值（找到的相应对象）
                         //method.invoke(instance,参数); 运行对象中的这个method
                }
            }
            //从二级缓存移除
//            earlySingletonObjects.remove(beanName);
            //从三级移除
            singleFactories.remove(beanName);

            //3. Aware回调
            // （容器会去调setBeanName，把UserService的bean名，传给这个方法
            if (instance instanceof BeanNameAware) {
                System.out.println("3.Aware回调");
                ((BeanNameAware) instance).setBeanName(beanName);
            }
            //后置处理1:（spring的扩展机制）
            //扫描所有beanPostProcessor实现类，运行具体处理流程
            for (BeanPostProcessor beanPostProcessor : beanPostProcessorList) {
                System.out.println("--后置处理1");
                instance = beanPostProcessor.postProcessBeforeInitialization(instance, beanName);
            }
            //4.初始化
            if (instance instanceof InitializeBean) {//他是InitializeBean,初始化
                System.out.println("4.初始化");
                try {
                    ((InitializeBean) instance).afterPropertiesSet();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            //后置处理2
            for (BeanPostProcessor beanPostProcessor : beanPostProcessorList) {
                System.out.println("--后置处理2");
                instance = beanPostProcessor.postProcessAfterInitialization(instance, beanName);
                //instance被修改了---> 代理对象(在核心基础上，加了辅助功能）(这个也要放在容器里吗？？？？？？？？)
                //代理对象没有再 依赖注入
            }


            if (earlySingletonObjects.containsKey(beanName)) {
                instance = earlySingletonObjects.get(beanName);//不要后面生成的代理
            }
            if("singleton".equals(beanDefinition.getScope())){
                singletonObjects.put(beanName,instance);
            }
            creatingBeanNameList.remove(beanName);//创好了
            return instance;

        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return null;

        // 其他流程：、、、、、、、、、、、、、、、、、、
    }




    //扫描
    public void scan(Class configClass){
        // （主要解析这个config配置类中spring提供的注解）
        Annotation declaredAnnotation=configClass.getDeclaredAnnotation(ComponentScan.class);//拿到注解
        // * 方法：isAnnotationPresent检查是否有注解，getDeclaredAnnotation获取注解

        ComponentScan componentScanAnnotation=(ComponentScan)declaredAnnotation;
        String path=componentScanAnnotation.value();//注解中的路径 //com.rqx.service
        //3. 扫描包里的’类‘ clazz
        //根据路径包名com.rqx.service得到所有类中带有spring注解的类
        //怎么找类：类加载器：BootStrap(jre/lib)，Ext(jar/lib), App(classpath目录下)
            // classpath:java执行文件路径
                //classpath ..\RqxSpring\target\classes com.rqx.Test com.rqx.service
        ClassLoader classLoader=RqxApplicationContext.class.getClassLoader();//App类加载器
        URL resource =classLoader.getResource(path.replace(".","/"));//classloader里拿"com/rqx/service"的资源(相对路径) ..\target\classes + 相对路径
        File file=new File(resource.getFile());
        if(file.isDirectory()) {//check是目录吗
            File[] files = file.listFiles();
            for (File f : files) {
                String fileName=f.getAbsolutePath();//文件路径..\RqxSpring\target\classes\com\rqx\service\UserService.class
                //--------------拿到类的类名 com.rqx.service.UserService
                String className=fileName.substring(fileName.indexOf("com"),fileName.indexOf(".class")).replace("\\",".");
                try {
                    Class<?> clazz  = classLoader.loadClass(className);
                    if (clazz.isAnnotationPresent(Component.class)) { //是否有component注解：需要创Bean
                        System.out.println("这个类需要创Bean对象："+clazz.getName());

                        // * 发现：这个类是Component 且实现BeanPostProcessor接口
                        if(BeanPostProcessor.class.isAssignableFrom(clazz)){
                            BeanPostProcessor instance=(BeanPostProcessor)clazz.getDeclaredConstructor().newInstance();//实例化这个对象
                            beanPostProcessorList.add(instance);//存起来
                        }
                        //* 检查获得aop的切面类
                        getPointcutFromAspect(clazz);


                        //4. 创建Bean对象,放进容器的map中（对有需求的类）
                        //BeanDefinition
                        Component component=clazz.getDeclaredAnnotation(Component.class);//拿到注解
                        String beanName=component.value();
                        BeanDefinition beanDefinition=new BeanDefinition();
                        if (clazz.isAnnotationPresent(Scope.class)) {
                            if (clazz.getDeclaredAnnotation(Scope.class).value().equals( "prototype")) {
                                // (2) 原型
                                Scope scopeAnnotation = clazz.getDeclaredAnnotation(Scope.class);
                                beanDefinition.setScope(scopeAnnotation.value());
                            } else {
                                // (1) 单例子：class -> bean(是否要生成bean对象), 放进map单例池
                                beanDefinition.setScope("singleton");
                            }
                        }else {
                            //默认单例
                            beanDefinition.setScope("singleton");
                        }
                        beanDefinition.setClazz(clazz);
                        beanDefinitionMap.put(beanName,beanDefinition); //扫描的所有bean的定义 放进来
                    }
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
        }
    }





    //其他人从容器拿bean
    public Object getBean(String beanName){
        //String -> Bean 对象//根据bean id找对应类（难点！！！！！！！）
        if (beanDefinitionMap.containsKey(beanName)){
            BeanDefinition beanDefinition=beanDefinitionMap.get(beanName);
            if (beanDefinition.getScope().equals("singleton")){
                Object o=singletonObjects.get(beanName);//得到bean对象
                if (o==null&&creatingBeanNameList.contains(beanName)) {
                    //正在创建：陷入了循环依赖
                    o = earlySingletonObjects.get(beanName);//单例池没有，去二级找
                }
                if(o==null&&creatingBeanNameList.contains(beanName)){
                    System.out.println("二级没有，三级执行放入二级");
                    //正在创建：陷入了循环依赖
                    o=singleFactories.get(beanName).getObject();
                    earlySingletonObjects.put(beanName,o);
                    singleFactories.remove(beanName);
                }
                if (o==null) o=createBean(beanName,beanDefinition);//二级没有，创
                return o;
            }
            else{//不是singleton,原型bean（每次都创建 新的）
                //创建bean对象
                return createBean(beanName,beanDefinition);
            }
        }else{
            //不存在bean
            throw new NullPointerException("beanDefination不存在bean");
        }
    }


    public void checkAOP(){
        if(configClass.isAnnotationPresent(EnableAspectJAutoProxy.class)){
            Class clazz= AnnotationAwareAspectJAutoProxyCreator.class;
            try {
                //AnnotationAwareAspectJAutoProxyCreator实例化
                AnnotationAwareAspectJAutoProxyCreator instance=(AnnotationAwareAspectJAutoProxyCreator) clazz.getDeclaredConstructor().newInstance();
                instance.setAfterMethodMap(afterMethodMap);
                instance.setBeforeMethodMap(beforeMethodMap);
                beanPostProcessorList.add(instance);
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }

        }

    }

    void getPointcutFromAspect(Class clazz){

        if(clazz.isAnnotationPresent(Aspect.class)){
            System.out.println(clazz.getName());
            System.out.println("加入aop map");
            for(Method declaredMethod:clazz.getDeclaredMethods()){
                if(declaredMethod.isAnnotationPresent(Before.class)){
                    String pointcut=declaredMethod.getDeclaredAnnotation(Before.class).value();
                    List<MethodClassInfo> methodClassInfoList=null;
                    if (beforeMethodMap.get(pointcut)==null)methodClassInfoList=new LinkedList<>();
                    else methodClassInfoList=beforeMethodMap.get(pointcut);
                    methodClassInfoList.add(new MethodClassInfo(clazz,declaredMethod));
                    System.out.println(pointcut);
                    beforeMethodMap.put(pointcut,methodClassInfoList);
                }
                if(declaredMethod.isAnnotationPresent(After.class)){
                    String pointcut=declaredMethod.getDeclaredAnnotation(After.class).value();
                    List<MethodClassInfo> methodClassInfoList=null;
                    if (afterMethodMap.get(pointcut)==null)methodClassInfoList=new LinkedList<>();
                    else methodClassInfoList=afterMethodMap.get(pointcut);
                    methodClassInfoList.add(new MethodClassInfo(clazz,declaredMethod));
                    System.out.println(pointcut);
                    afterMethodMap.put(pointcut,methodClassInfoList);
                }
            }
        }
    }


    private Object getEarlyBeanReference(String beanName,Object bean){
        Object exposedObeject=bean;
        for(BeanPostProcessor beanPostProcessor:beanPostProcessorList){
            if(beanPostProcessor instanceof AnnotationAwareAspectJAutoProxyCreator){
                //代理处理
                exposedObeject=beanPostProcessor.postProcessAfterInitialization(bean,beanName);
            }
        }
        return exposedObeject;
    }


}
