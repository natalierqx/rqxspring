package com.spring.allInterface;

public interface BeanPostProcessor {
    Object postProcessBeforeInitialization(Object bean,String beanName);
    Object postProcessAfterInitialization(Object bean,String beanName);
}

//BeanPostProcessor接口 bean后处理：
//      postProcessBeforeInitialization()  初始前
//      postProcessAfterInitialization()   初始化后
//      子接口：InstantiationAwareBeanPostProcessor接口: 实例化回调
//              postProcessorBeforeInstantiation() 实例化前
//              postProcessorAfterInstantiation() 实例化后
//              postProcessAfterPropertie() 属性赋值后