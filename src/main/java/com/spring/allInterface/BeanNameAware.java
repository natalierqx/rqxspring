package com.spring.allInterface;

public interface BeanNameAware {
    public void setBeanName(String name);
}
