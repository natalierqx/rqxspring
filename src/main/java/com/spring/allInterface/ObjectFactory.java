package com.spring.allInterface;

@FunctionalInterface
public interface ObjectFactory {
    Object getObject();
}
