package com.spring.allInterface;

public interface InitializeBean {
    void afterPropertiesSet() throws Exception;
}
