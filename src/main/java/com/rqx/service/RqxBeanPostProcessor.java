package com.rqx.service;

import com.spring.allInterface.BeanPostProcessor;
import com.spring.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

//创建任何bean都会进入两个方法
//spring容器：实际上
//@Component("rqxBeanPostProcessor")//这个也是bean，但是实现了BeanPostProcessor接口
public class RqxBeanPostProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        //**做什么都可以，AOP利用这种机制
        //针对某个bean处理 灵活
        if (beanName.equals("userService")) {
            ((UserServiceImpl)bean).setBeanName("初始化前userService");//运行哪个方法（也是要去扫描的 扫描PostConstruct）
        }
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {

        if (beanName.equals("userService")) {
            ((UserServiceImpl)bean).setBeanName("初始化后userService");//运行哪个方法（也是要去扫描的 扫描PostConstruct）
        }

//        //这里用JDK(需要接口)     【 CGlib（需要jar包）
//        if (beanName.equals("myService")) {
//            //创建代理对象
//            Object proxyInstance =Proxy.newProxyInstance(RqxBeanPostProcessor.class.getClassLoader(),//1. 类加载器
//                    bean.getClass().getInterfaces(),//2. 目标类(bean对象的类)的接口（核心功能）
//                    new InvocationHandler() { //拦截
//                        @Override
//                        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
//                            System.out.println("切面逻辑， 辅助功能");// @Before, @After
//                            return method.invoke(bean,args);
//                        }
//                    });//3. 辅助功能
//
//            // 返回的代理对象（实现目标的接口），增添辅助功能，（但没有目标对象的属性值信息，是新的对象）
//            return proxyInstance; //替换对象
//        }

        return bean;
    }
}
