package com.rqx.service;


import com.spring.Autowired;
import com.spring.Component;
import com.spring.Scope;

//业务:  需要注解@component @Controller @Service

//定义Bean，放进容器
@Component("orderService") //
public class OrderServiceImpl implements OrderService {
    @Autowired
    private UserService userService;

    @Override
    public UserService testUser() {
        return userService;
//        System.out.println("测试注入user"+userService);
    }
}
