package com.rqx.service;

import com.spring.Component;
import com.spring.aop.After;
import com.spring.aop.Aspect;
import com.spring.aop.Before;

@Component("log")
@Aspect
public class Log {


//    @Before("userService.queryUser")
    @Before("userService.testOrder")
    //前置事务---切入testOrder方法
    public void logBefore(){
        System.out.println("业务之前的日志");
    }

//    @After("userService.queryUser")
    @After("userService.testOrder")
    //后置事务---切入testOrder方法
    public void logAfter(){
        System.out.println("业务之后的日志");
    }


}
