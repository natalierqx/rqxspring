package com.rqx.service;


import com.spring.*;
import com.spring.allInterface.BeanNameAware;
import com.spring.allInterface.InitializeBean;

//业务:  需要注解@component @Controller @Service

//定义Bean，放进容器
//2. scope:
//  原型bean：不同对象（在使用时创建）
//  单例bean默认：拿到的bean对象是同一个   spring里的单例池 map<beanid,bean对象>
//  <bean id="addr" class="com.qf.entity.Address" scope="prototype">
@Component("userService") // （这里没有默认值，spring中会解析类型名称首字母小写） //@service区别
//@Scope("prototype")
public class UserServiceImpl implements UserService, InitializeBean, BeanNameAware {
    @Autowired//从容器 拿orderService放进下面
    private OrderServiceImpl orderService;
    @Override
    public OrderService testOrder(){
        return orderService;
//        System.out.println("测试注入order"+orderService);
    }



//*sping的推断构造:
    //spring实例化对象：默认找无参构造方法
    //               定义了一个构造方法：（spring就会找这个方法实例化对象，但是有入参（他就去容器来中找一个orderservice入参，或者创一个）
    //                          找：by类型，by name
    //               定义了多个构造方法：报错，解决：@autowired去指定
    //这时候this.orderService 没有Authoward还是会被赋值
//    public UserService(OrderService orderService){
//        this.orderService=orderService;
//    }
//============================-========================================================
    // @bean注解
    // 告诉方法,产生一个Bean对象(AA),然后这个Bean对象交给Spring管理。
    // 产生这个Bean对象的方法aa3, Spring只会调用一次,随后这个Spring将会将这个Bean对象(aa3)放在自己的IOC容器中
    // public AA aa3(){return new AA()} //


//=======================================================-===========================================
    //BeanNameAware接口： 实现这个接口的Bean可以通过setBeanName接收到自己的名字
    private String beanName;//拿到容器中UserService bean的名字userService
    @Override
    public void setBeanName(String name) {
        beanName=name;
    }
    @Override
    public void printBeanName(){
//        System.out.println(beanName);
    }



    //InitializeBean初始化bean接口: spring容器，创建bean时调用afterPropertiesSet方法
    @Override
    public void afterPropertiesSet() throws Exception {
//        System.out.println("初始化");
    }







//=======================================================-===========================================
    //测试AOP：声明一个核心业务
    @Override
    public void queryUser(){
        System.out.println("查询用户");
    }
}
