package com.rqx;

import com.rqx.service.MyInterface;
import com.rqx.service.OrderService;
import com.rqx.service.UserService;
import com.spring.RqxApplicationContext;

public class Test {
    public static void main(String[] args) {
        RqxApplicationContext applicationContext=new RqxApplicationContext(AppConfig.class);
//1.测单例和原型
//        System.out.println
//        System.out.println(applicationContext.getBean("userService"));
//        System.out.println(applicationContext.getBean("userService"));

//2.测UserService中注入的OrderService对象
//        UserService userService=(UserService) applicationContext.getBean("userService");
//        userService.testOrder();

//3.测试Aware回调、测试BeanPostProcessor改名
//        userService.printBeanName();


//4.测试循环依赖：引入二级缓存
//        UserService userService=(UserService) applicationContext.getBean("userService");
//        userService.testOrder();
//        System.out.println(userService);
//        OrderService orderService=(OrderService) applicationContext.getBean("orderService");
//        orderService.testUser();
//        System.out.println(orderService);

// 5.测试AOP
//        UserService userService=(UserService) applicationContext.getBean("userService");
//        userService.queryUser();

// 6.测试AOP对循环依赖影响：引入三级缓存
        UserService userService=(UserService) applicationContext.getBean("userService");
        OrderService orderService=(OrderService) applicationContext.getBean("orderService");
        userService.testOrder().testUser();
        // 创U，注入O（创O，注入u），得到O，得到U。
        orderService.testUser().testOrder();

















//        MyInterface myService =(MyInterface) applicationContext.getBean("myService");
//        myService.test();
    }
}
